#### ==== Basics ====

Top 10 Riders (done)
Top 100 riders (done)

Distribution of past weather? (Done)
Distribution of future weather? <-- How?!

Started / Won Rate for each rider? (Done)

How often does challenger win? (done)
How often does opponnent win? (done)

Which rider has raced how many times? (done)

Distribution of past race tracks? (done)

Distribution of future race tracks? (done)

#### ==== Rider Analysis ====


Which driver wins against which drivers how often? (done)


What has the most influence: track, opponents or weather? (done)


Development of the drivers? Wins over time? (done)


####  ==== Predictions ====

Using multiple models to predict which rider will win future races (done)

Which criterion plays a role for which driver? (done)
