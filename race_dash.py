### Import require libraries ###
import pandas as pd
import numpy as np

# # Visualisation
# import matplotlib as mpl
# import matplotlib.pyplot as plt
# import matplotlib.pylab as pylab
# import seaborn as sns

# Configure visualisations
# %matplotlib inline
# mpl.style.use( 'ggplot' )
# sns.set_style( 'whitegrid' )
# pylab.rcParams[ 'figure.figsize' ] = 8 , 6

# Dashboard
import dash
import dash_core_components as dcc
import dash_html_components as html
import time
import plotly.graph_objs as go
import plotly.express as px
import random
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import dash_table
import requests
import locale
locale.setlocale(locale.LC_ALL, '')

# Machine learning
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics

# Create App
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server
# Allow multiple tabs
app.config['suppress_callback_exceptions'] = True

#TODO:
# Create controls 

### Load data & data preparation ###
df = pd.read_csv('races.csv', delimiter=';')
df['fuel_consumption'] = pd.to_numeric(df['fuel_consumption'], errors='coerce')

#TODO: (Check if we need this later)
# Split into finished and scheduled races
df_waiting = df[df.status.eq('waiting')]
df_finished = df[df.status.eq('finished')]
df_declined = df[df.status.eq('declined')]
df_retired = df[df.status.eq('retired')]

### Calculation Functions ###
def get_races():
    df_drivers_1 = pd.DataFrame()
    df_drivers_1['id'] = df_finished['challenger']
    df_drivers_2 = pd.DataFrame()
    df_drivers_2['id'] = df_finished['opponent']
    df_drivers = df_drivers_1.append(df_drivers_2)
    df_drivers = df_drivers['id'].value_counts().to_frame().rename(columns={'index':'driver', 'id':'result'})
    df_drivers['id'] = df_drivers.index.values
    return df_drivers

def get_wins():
    df_winners=df_finished['winner'].value_counts().to_frame().rename(columns={'index':'driver', 'winner':'result'})
    df_winners['id']=df_winners.index.values
    return df_winners

def calc_driven_win_rate(driverid):
    df_chall = df_finished[df_finished['challenger']==driverid]
    df_opp = df_finished[df_finished['opponent']==driverid]
    df_win = df_finished[df_finished['winner']==driverid]
    return len(df_win)/(len(df_chall)+len(df_opp))

def get_rate():
    df_drivers_1 = pd.DataFrame()
    df_drivers_1['id'] = df_finished['challenger']
    df_drivers_2 = pd.DataFrame()
    df_drivers_2['id'] = df_finished['opponent']
    df_drivers = df_drivers_1.append(df_drivers_2)
    df_drivers2 = df_drivers.drop_duplicates()
    df_drivers2['result'] = df_drivers2['id'].apply(calc_driven_win_rate)
    return df_drivers2.sort_values(by='result', ascending = False)

# Prepare dfs for ranglist
df_races = get_races()
df_wins = get_wins()
df_wl_rate = get_rate()

### Create Layout ###
app.layout = html.Div([
    html.Div(
            [
                html.Div(
                    dbc.Row(
                        [
                            dbc.Col(
                                width=4
                            ),
                            dbc.Col(
                                html.H1('Racing Analytics'),
                                width=4
                            ),
                            dbc.Col(
                                width=4
                            )
                        ],
                        align='start',
                        justify='between',
                    )
                ),
            ],
        ),

    dcc.Tabs(id='tabs', value='tab-1', children=[
        dcc.Tab(label='Overview', value='tab-1', className='custom-tab', selected_className='custom-tab--selected'),
        dcc.Tab(label='Details', value='tab-2', className='custom-tab', selected_className='custom-tab--selected'),
    ]),
    html.Div(id='tabs-content')
])

# First Tab
@app.callback(dash.dependencies.Output('tabs-content', 'children'),
              [dash.dependencies.Input('tabs', 'value')])
def render_content(tab):
    if (tab == 'tab-1'):
        return html.Div(
            [
                dbc.Row(
                    [
                        dbc.Col(
                            html.Div(
                                [
                                    html.P(
                                        'No. of finished races'
                                    ),
                                    html.H6(
                                        f'{len(df_finished):n}',
                                        className='info_text')
                                ],
                                className='pretty_container'
                            )
                        ),

                        dbc.Col(
                            html.Div(
                                [
                                    html.P(
                                        'No. of waiting races'
                                    ),
                                    html.H6(
                                        f'{len(df_waiting):n}',
                                        className='info_text')
                                ],
                                className='pretty_container'
                            )
                        ),

                        dbc.Col(
                            html.Div(
                                [
                                    html.P(
                                        'No. of declined races'
                                    ),
                                    html.H6(
                                        f'{len(df_declined):n}',
                                        className='info_text')
                                ],
                                className='pretty_container'
                            )
                        ),

                        dbc.Col(
                            html.Div(
                                [
                                    html.P(
                                        'No. of retired races'
                                    ),
                                    html.H6(
                                        f'{len(df_retired):n}',
                                        className='info_text')
                                ],
                                className='pretty_container'
                            )
                        )
                    ]
                ),

            dbc.Row(
                [
                    dbc.Col(
                        html.Div(
                            [
                                html.P(
                                    'Select type of ranking:',
                                    className='control_label'
                                ),
                                dcc.Dropdown(
                                    id='ranglist_selector',
                                    options=[
                                        {'label': 'Most races     ', 'value': 'races'},
                                        {'label': 'Most wins     ', 'value': 'wins'},
                                        {'label': 'Highest win/loss rate     ', 'value': 'rate'},
                                    ],
                                    multi=False,
                                    value='races',
                                ),
                                html.P(
                                    'Select the ranking area:',
                                    className='control_label'
                                ),
                                dcc.RangeSlider(
                                    id='ranglist_slider',
                                    count=1,
                                    min=0,
                                    max=5749,
                                    step=1,
                                    value=[0,10]
                                ),
                                dcc.Graph(
                                    id='ranking_graph'
                                )
                            ],
                            className='pretty_container'
                        )
                    )
            ])

        ]),

        dbc.Row(
            [
                dbc.Col(
                    html.Div(
                        [

                        ]
                    )
                ),

                dbc.Col(
                    html.Div(
                        [
                            
                        ]
                    )
                )
            ]
        )

### Create callbacks ###
@app.callback(
    Output('ranking_graph', 'figure'),
    [Input('ranglist_selector', 'value'), Input('ranglist_slider','value')]
)
def draw_ranking_graph(ranglist_selector,ranglist_slider):
    dff = df_races
    fig_title = 'Most races driven'
    x_axis_title = 'Ranking'
    y_axis_title = 'Number of races'

    if (ranglist_selector == 'races'):
        dff = df_races
        y_axis_title = 'Number of races'
    
    elif (ranglist_selector == 'wins'):
        dff = df_wins
        fig_title = 'Most races won'
        y_axis_title = 'Number of wins'
    
    elif (ranglist_selector == 'rate'):
        dff = df_wl_rate
        fig_title = 'Highest win/loss quote'
        y_axis_title = 'Wins/Total'
    
    dff = dff[ranglist_slider[0]:ranglist_slider[1]]
    fig = px.bar(dff, y="result", hover_data=["id"] ,height=400)
    fig.update_layout(
    title=fig_title,
    xaxis_title=x_axis_title,
    yaxis_title=y_axis_title,
    font=dict(
        size=16,
        color="#7f7f7f"
    ),
    )

    fig.update_layout({
        'plot_bgcolor': 'rgba(0, 0, 0, 0)',
        'paper_bgcolor': 'rgba(0, 0, 0, 0)',
        })

    return fig


# main
if __name__ == '__main__':
    app.run_server(debug=True)